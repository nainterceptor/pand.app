+++
title = "Cloner tous les dépots d'un sous groupe gitlab"
date = 2020-11-30
category = "Gitlab"

[taxonomies]
tags = ["IT", "Code", "Gitlab"]
+++

Aujourd'hui, j'ai dû cloner tous les dépôts d'un groupe gitlab. Il y en avait 17. Ça m'a saoulé. J'ai entrepris de faire autrement.

## Prérequis

- Si jamais votre clef ssh a une passphrase, pensez à faire `ssh-add`, ça vous évitera de rentrer la passphrase pour chaque dépot.
- `export GITLAB_TOKEN=<votre jeton gitlab>`
- `export GITLAB_GROUP=<id du groupe gitlab>`

## Commande

```bash
curl --silent --header "Private-Token: $GITLAB_TOKEN" \
 https://gitlab.com/api/v4/groups/$GITLAB_GROUP/projects\?include_subgroups=true \
 | jq -r '.[].ssh_url_to_repo' | xargs -I _ git clone _
```

Informations :
- `--silent` de `curl` est là pour éviter de voir le téléchargement curl
- remplacez gitlab.com éventuellement par votre instance gitlab
- `-r` de `jq` est là pour éviter les quotes dans la sortie jq
- `.[].ssh_url_to_repo` va chercher la clef `ssh_url_to_repo` dans chaque sortie du tableau json envoyé par curl
- `xargs -I _` va exécuter la commande qui suit une fois par ligne remontée par jq, remplaçant `_` par la ligne en question.
